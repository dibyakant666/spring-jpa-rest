package com.demo.springjpa.dbconn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.springjpa.dbconn.entity.User;
import com.demo.springjpa.dbconn.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository repository;
	
	public User saveUser(User user) {
		user = repository.save(user);
		return user;
	}
	public List<User> saveUsers(List<User> users) {
		return repository.saveAll(users);
	}
	public List<User> getUsers() {
		return repository.findAll();
	}
	public User getUsersById(int userId) {
		User user = repository.findById(userId).orElse(null);
		return user;
	}
	public String deleteProduct(int id) {
		repository.deleteById(id);
		return "User ID "+ id + " removed ";
	}
	
	
}
