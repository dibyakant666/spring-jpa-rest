package com.acc.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.acc.app.entity.Book;
import com.acc.app.service.GraphQLService;

import graphql.ExecutionResult;

@RestController
public class BookController {
	
	@Autowired
	GraphQLService graphQLService;
	
	@PostMapping("/addbook")
	public Book addUser(@RequestBody Book book) {		
		return graphQLService.saveUser(book);
	}
	
	@PostMapping("/books")
	public ResponseEntity<Object> getAllBooks(@RequestBody String query) {
		ExecutionResult execute = graphQLService.getGraphQL().execute(query);
		return new ResponseEntity<>(execute, HttpStatus.OK);
	}
}
