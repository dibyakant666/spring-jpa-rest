package com.demo.springjpa.dbconn.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Qualifications {
	
	@Id
	@SequenceGenerator(
			name="qual_seq",
			sequenceName="qual_seq",
			allocationSize=1
			)
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="qual_seq"
			)
	private int qualId;
	private String qualDetails;
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(
//			name="user_id",
//			referencedColumnName="userId"
//			)
//	private User user;
	public int getQualId() {
		return qualId;
	}
	public void setQualId(int qualId) {
		this.qualId = qualId;
	}
	public String getQualDetails() {
		return qualDetails;
	}
	public void setQualDetails(String qualDetails) {
		this.qualDetails = qualDetails;
	}
//	public User getUser() {
//		return user;
//	}
//	public void setUser(User user) {
//		this.user = user;
//	}
}
